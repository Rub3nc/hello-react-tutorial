var React = require('react');
var ReactDOM = require('react-dom');
var Greeter = require('Greeter');
var name = "Penguin";
var my_message = "This is my custom message!";

ReactDOM.render(
  <Greeter name={name} message={my_message} />,
  document.getElementById('app')
)
