# Babel
Babel let us use ES6 features and JSX directly into our code,
and compile it into a javascript supported by all the browsers.

# React Component definition
We do this by using the **createClass** method from the **React class**.
The only mandatory attribute is the render function.
The render function MUST return just one root element, if we try to return
multiple root elements we will get an error.


# React Props
Which is short for Properties, It is a way to pass data into our components when
you first start it.

To access the Properties passed to our components we have to pull it out via
the **this.props object** inside the **render** function.

## Example
```javascript
  var Greeter = React.createClass({
    render: function(){
      var name = this.props.name;

      return (
        <div>
          <h1>Hello {name}!</h1>
          <p>This is from a component!</p>
        </div>
      )
    }
  });

  ReactDOM.render(
    <Greeter name="Penguin"/>,
    document.getElementById('app')
  )
```
## Default values to our props
Now this component is more useful because we can pass some data to it. But we
don't pass anything it still is going to print "Hello !" and that is because
our name props is empty. to fix this we use a method that comes built-in with
React called **getDefaultProps** with return a default object that will have our
default values to our component.

## Example
```javascript
var Greeter = React.createClass({
  getDefaultProps: function(){
    return {
      name: 'React'
    }
  },
  render: function(){
    var name = this.props.name;

    return (
      <div>
        <h1>Hello {name}!</h1>
        <p>This is from a component!</p>
      </div>
    )
  }
});

ReactDOM.render(
  <Greeter name="Penguin"/>,
  document.getElementById('app')
)
```

In that way if we don't pass the **name** property our component is going to set
the default value to **React**.

We can pass variable to the props of our components interpolating it inside {}.
```javascript
  var name = "Penguin";

  ReactDOM.render(
    <Greeter name="{name}"/>,
    document.getElementById('app')
  )
```

## Example passing multiple Properties to our component
```javascript
var Greeter = React.createClass({
  getDefaultProps: function(){
    return {
      name: 'React',
      message: 'This is from a component!'
    }
  },
  render: function(){
    var name = this.props.name;
    var message = this.props.message;

    return (
      <div>
        <h1>Hello {name}!</h1>
        <p>{message}</p>
      </div>
    )
  }
});

var name = "Penguin";
var my_message = "This is my custom message!";

ReactDOM.render(
  <Greeter name={name} message={my_message} />,
  document.getElementById('app')
)
```


# Props vs state
State can be changed, Props Can't.
Container Components have state, Presentational Components Don't.
Our Container components shouldn't do much rendering other than rendering its children.


# Webpack - Generating Bundles
Problems with out current project "index.html" has to track all the javascript tags or dependencies.
Our app.jsx is compiled and loaded at run-time, which makes it very slow, this is done by the
type="text/babel" tag.

## First bundle with webpack:
To ilustrate how webpack works, first we have to make some changes in our **index.html** file:
```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <div id="app"></div>
    <script src="bundle.js"></script>
  </body>
</html>
```

Look that we changed the app.jsx to bundle.js, this bundle.js file is the one we are going to be
generating through webpack.

Then we create the file "public/app.js" with the following content.
```javascript
var greeter = require('./components/Greeter');
greeter();
```

Look at the require statement, this is says, take anything the the module **./components/Greeter**
(We can exclude the extension of the file at this moment.)
exports en then save them in the greeter variable. then execute the greeter function.


Let's have a look at the **./components/Greeter.js** file:
```javascript
function greeter() {
  document.write('From greeter function!');
}

module.exports = greeter;
```
Look at the bottom of the file we set the module.exports to whatever we want to
export from this module, and make it accessible by other modules that use the
require statement, in this case we export the greeter function.

Now we can generate our bundle:
```sh
webpack ./public/app.js ./public/bundle.js
```
this will take the *./public/app.js* and compile it to our *bundle.js* file.
Now re-run the server and head over *http://localhost:3000* to see our working
demo.

With this structure we can now, split our react components through several files
and make our application more scalable and testable, because we will not have
that long file with all the code in it.

## The Webpack config file
In the previous part of the tutorial we used the command *webpack ./public/app.js ./public/bundle.js*
to generate our bundle using webpack. We gave it the file where the application starts
and it went through *app.js* and found for us the things it requires and put it all together
in *bundle.js*. That was fine but our application uses JSX and ES6 and then we need
to switch a little bit how we do things with webpack. Instead of running from the terminal
and giving it an output and input file, we need to specify a config file were we can pass
in various plugins like Babel.

In order to do this,  we have to create a new file called *webpack.config.js* in our root
directory. This file export an object, and that object let us configure what webpack does.
Our *webpack.config.js* look like this:

```javascript
module.exports = {
  entry: './public/app.jsx',
  output: {
    path: __dirname,
    filename: './public/bundle.js'
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  }
};
```

### Properties:
- **entry:** this tells webpack where it should start processing our code *(in our case is ./public/app.js)*
- **output:**: is an object with to properties *path* and from that folder we specify the output file with the property *filename*.
- **resolve:** it is going to take an extensions array, that is a list of file extensions we want to process.

Now we can run the command webpack in the command-line without any arguments, in that way webpack is going to go
through the config file and grab everything and put it all together.

But in this way we still can't use es6 or react in our js or jsx file.
By default webpack does not know what to do with the jsx files in order to configure what it should do with those files we need to activate babel in our *webpack.config.js*

We are going to use Babel loader to convert all our jsx files into ES5 code.
We need to add our loader into the modules attribute
```javascript
module.exports = {
  entry: './public/app.jsx',
  output: {
    path: __dirname,
    filename: './public/bundle.js'
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  module: {
    loaders: [
      {
        loader: 'babel-loader',
        query: {
          presets: ['react', 'es2015']
        },
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/
      }
    ]
  }
};
```
We add a few changes into our files:
- In the module object we add the loaders attribute and this is going to be an array of loader objects.
- The query object tell that it should process our files through react and then through es2015.
- The test attribute is a regular expression where we specify the extension of the file we want to process.
- The exclude is a regular expression where we tell webpack which folder to ignore when processing our files.
- In our *app.jsx* file, because we remove the script tags from our *index.html* file the *React* en *ReactDOM* no longer exists we have to add the next lines into our *app.jsx* file *(remember that this dependencies where installed via npm)*.

```javascript
var React = require('react');
var ReactDOM = require('react-dom');
```


To stop using the relative path in our require statements we have to make some changes to our webpack config file:
```javascript
module.exports = {
  entry: './public/app.jsx',
  output: {
    path: __dirname,
    filename: './public/bundle.js'
  },
  resolve: {
    root: __dirname,
    alias: {
      Greeter: 'public/components/Greeter.jsx',
      GreeterMessage: 'public/components/GreeterMessage.jsx',
      GreeterForm: 'public/components/GreeterForm.jsx'
    },
    extensions: ['', '.js', '.jsx']
  },
  module: {
    loaders: [
      {
        loader: 'babel-loader',
        query: {
          presets: ['react', 'es2015']
        },
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/
      }
    ]
  }
};
```

The only thing that changed was the resolve object, when we set the root directory from where it will resolve and then we set some alias when we specify the relative path to each of our components and in that way we don't have to use paths anymore
in our jsx files.

to avoid running webpack command everytime we make some changes, we can run the webpack with the -w flag, as it follows:
```sh
webpack -w
```


# Boilerplate Project
This is the Boilerplate we develop using this tutorial *git@gitlab.com:Rub3nc/simple-react-boilerplate.git*
